tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : ((e=expr {drukuj ($e.text + " = " + $e.out.toString());}) | varinit)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = subtract($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = multiply($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = setSymbolValue($i1.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getSymbol($ID.text);}
        ;

varinit 
        : ^(VAR   i1=ID)           {addSymbol($ID.text);} ; 