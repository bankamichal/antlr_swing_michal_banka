package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	private GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

  	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer subtract(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer multiply(Integer a, Integer b) {
		return a * b;
	}
	
	protected Integer divide(Integer a, Integer b) {
		return a / b;
	}
	
	protected void addSymbol(String name) {
		globalSymbols.newSymbol(name);
	}
	
	protected Integer setSymbolValue(String name, Integer value) {
		globalSymbols.setSymbol(name, value);
		return value;
	}
	
	protected Integer getSymbol(String name) {
		return globalSymbols.getSymbol(name);
	}
}
